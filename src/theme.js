import {
  red700,
  grey300,
  grey400,
  grey600,
  grey900,
  pinkA100, pinkA200, pinkA400,
  fullWhite,
  grey50
} from 'material-ui/styles/colors';
import {fade} from 'material-ui/utils/colorManipulator';
import spacing from 'material-ui/styles/spacing';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

export default getMuiTheme({
  userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.82 Safari/537.36',
  spacing: spacing,
  fontFamily: 'Roboto, sans-serif',
  palette: {
    primary1Color: red700,
    primary2Color: red700,
    primary3Color: grey600,
    accent1Color: pinkA200,
    accent2Color: pinkA400,
    accent3Color: pinkA100,
    textColor: fullWhite,
    secondaryTextColor: fade(fullWhite, 0.7),
    alternateTextColor: '#303030',
    canvasColor: '#303030',
    borderColor: fade(fullWhite, 0.3),
    disabledColor: fade(fullWhite, 0.3),
    pickerHeaderColor: fade(fullWhite, 0.12),
    clockCircleColor: fade(fullWhite, 0.12),
  },
  appBar: {
    display: '-webkit-flex',
    color: fullWhite,
    textColor: grey300
  },
  svgIcon:
  {
    color: grey400
  },
  tabs: {
    backgroundColor: fullWhite,
    selectedTextColor: red700,
    textColor: grey400
  },
  inkBar: {
    backgroundColor: grey400
  },
  avatar: {
    backgroundColor: grey300,
    color: grey900,
    size: 20
  },
  chip: {
    backgroundColor: grey300,
    textColor: grey600
  },
  paper: {
    backgroundColor: fullWhite,
    zDepthShadows: 0
  },
  card: {
    titleColor: grey900,
    subtitleColor: grey900

  },
  cardText: {
    textColor: grey900
  },
  raisedButton: {
    textColor: fullWhite,
    primaryTextColor: fullWhite,
    fontSize: 18,
    textTransform: 'none'
  },
  floatingActionButton: {
    color: red700,
    iconColor: fullWhite,
    buttonSize: 100,
  }

});

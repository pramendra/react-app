export function fetchListItem(id) {
  return {
    type: 'GET_LISTITEM_DATA',
    id
  }
}

/* eslint react/prefer-stateless-function: 0, react/forbid-prop-types: 0 */
/* eslint global-require: 0 */

import React from 'react';

import theme from './../theme';
import { MuiThemeProvider} from "material-ui/styles";
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import styles from './App.css';


class App extends React.Component {
  childContextTypes = {
    muiTheme: React.PropTypes.object.isRequired,
  }
  getChildContext() {
    return {muiTheme: theme};
  }
  render() {
    return (
      <MuiThemeProvider muiTheme={theme}>
        <div className={styles.app}>
          {this.props.children}
        </div>
      </MuiThemeProvider>
    );
  }
}
App.childContextTypes = {
  muiTheme: React.PropTypes.object.isRequired,
};

export default App;

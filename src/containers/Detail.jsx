import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import DetailComponent from './../components/pages/detail/Index';
import * as ListItemActionCreators from './../actions/ListItemActionCreators'

class Detail extends Component {
  static propTypes = {
    listitem: PropTypes.object.isRequired
  };
  componentDidMount() {
    const { listitemActions } = this.props;
    listitemActions.fetchListItem(this.props.params.id);
  }
  render(){
    return (
      <DetailComponent listitem={this.props.listitem}/>
    );
  }
}

function mapStateToProps(state) {
  return {
    listitem: state.listitem
  };
}

function mapDispatchToProps(dispatch) {
  return {
    listitemActions: bindActionCreators(ListItemActionCreators, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Detail);

import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import HomeComponent from './../components/pages/home/Index';
import * as ListActionCreators from './../actions/ListActionCreators'
import * as CategoryActionCreators from './../actions/CategoryActionCreators'



class Home extends Component {

  static propTypes = {
    listActions: PropTypes.object.isRequired,
    categoryActions: PropTypes.object.isRequired
  };

  componentDidMount() {
    const { listActions, categoryActions } = this.props;
    listActions.fetchList();
    categoryActions.fetchList();
  }

  render(){
    return (
      <HomeComponent
        list = {this.props.list}
        categories = {this.props.categories}
      />
    );
  }
}
function mapStateToProps(state) {
  return {
    list: state.list,
    categories: state.categories
  };
}

function mapDispatchToProps(dispatch) {
  return {
    listActions: bindActionCreators(ListActionCreators, dispatch),
    categoryActions: bindActionCreators(CategoryActionCreators, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);

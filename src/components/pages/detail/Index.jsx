import React, { Component } from 'react';
import Avatar from 'material-ui/Avatar';
import Chip from 'material-ui/Chip';
import Subheader from 'material-ui/Subheader';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import { brown900, fullWhite, grey300 } from 'material-ui/styles/colors';

import ActionFavoriteBorder from 'material-ui/svg-icons/action/favorite-border';
import CommunicationChatBubbleOutline from 'material-ui/svg-icons/communication/chat-bubble-outline';
import ImageAssistantPhoto from 'material-ui/svg-icons/image/assistant-photo';
import AppBarWithBackButton from './../../common/AppBarWithBackButton';
import stylesApp from './../../../containers/App.css';

const cartTextItemsWrapperSpacing = 10;
const styles = {
  chip: {
    margin: 4,
  },
  wrapper: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  button: {
    marginRight: 20,
  },
  buyActionWrapper: {
    position: 'fixed',
    bottom: 0,
    left: 0,
    padding: 5,
    width: '100%',
    backgroundColor: brown900,
    textColor: fullWhite
  },
  cardText: {
    padding: 0
  },
  cardSubTitle: {
    padding: cartTextItemsWrapperSpacing,
    paddingBottom: 0,
    fontSize: 16
  },
  metaInfoNumber: {
    padding: cartTextItemsWrapperSpacing,
  },
  metaInfo: {
    padding: cartTextItemsWrapperSpacing,
    backgroundColor: grey300,
  },
  buyButtonWrapper: {
    textAlign: 'right',
    marginRight: 20
  },
  buyActionPrice: {
    color: fullWhite
  },
  buybutton: {
    height: 40,
    width: 40,
  }
};

const formatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
  minimumFractionDigits: 0,
});


export default class Index extends Component {

  render() {
    const product = this.props.listitem;

    return (
      <div>
        <AppBarWithBackButton name={product.name}/>
        <Card>
          <CardMedia>
            <img src={product.image} />
          </CardMedia>
          <CardTitle subtitleStyle={styles.cardSubTitle} subtitle={product.name} />
          <CardText style={styles.cardText}>

            <div style={styles.metaInfoNumber} className={stylesApp.Grid + ' ' + stylesApp.GridCenter}>
              <div className={stylesApp.GridCell75}>
                <div className={stylesApp.Grid + ' ' + stylesApp.GridCenter}>
                  <div className={stylesApp.GridCell}>
                    <Chip style={styles.chip} >
                      <Avatar icon={<ActionFavoriteBorder />} />
                        Like {product.like_count}
                    </Chip>
                  </div>
                  <div className={stylesApp.GridCell}>
                    <Chip style={styles.chip} >
                      <Avatar icon={<CommunicationChatBubbleOutline />} />
                        Comment {product.comment_count}
                    </Chip>
                  </div>
                </div>
              </div>
              <div className={stylesApp.GridCell25}>
                <Avatar icon={<ImageAssistantPhoto />} />
              </div>
            </div>

            <div style={styles.metaInfo}>
              <div style={styles.description}>
                {product.description}
              </div>

              <div style={styles.shippingFee}>
                {product.shippingFee}
              </div>

            </div>

          </CardText>
          <CardActions>
            <div style={styles.buyActionWrapper}>
              <div className={stylesApp.Grid + ' ' + stylesApp.GridCenter}>
                <h2 style={styles.buyActionPrice} className={stylesApp.GridCell}>
                  US{formatter.format(product.price)}
                </h2>
                <div style={styles.buyButtonWrapper} className={stylesApp.GridCell}>
                  <RaisedButton label="Buy" primary={true} style={styles.buybutton} />
                </div>
              </div>

            </div>
          </CardActions>
        </Card>
      </div>
    );
  }
}

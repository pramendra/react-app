/* eslint react/prefer-stateless-function: 0 */

import React, { Component } from 'react';
import { connect } from 'react-redux'
import AppBarWithDrawer from './../../common/AppBarWithDrawer';
import CategoryNav from './../../common/CategoryNav';

import CircularProgressBars from './../../common/CircularProgressBars'
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ImageCameraAlt from 'material-ui/svg-icons/image/camera-alt';
import { fullWhite, red700 } from 'material-ui/styles/colors';

// {this.props.loading ? <CategoryNav />: <CircularProgressBars /> }


const styles = {
  sellActionWrapper: {
    position: 'fixed',
    bottom: -15,
    right: -15,
  },
  buttonWrapper: {
    clolor: fullWhite
  },
  sellText: {
    color: fullWhite,
    fontSize: 16
  },
  iconStyle: {
    width: 50,
    height: 50,
    color: fullWhite
  }
}
class Index extends Component {

  render() {
    return (
      <div>
        <AppBarWithDrawer />
        <CategoryNav
          list = {this.props.list}
          categories = {this.props.categories}
        />
      <div style={styles.sellActionWrapper}>
        <FloatingActionButton style={styles.buttonWrapper}>
            <div style={styles.sellText}>Sell</div>
            <ImageCameraAlt style={styles.iconStyle}/>
        </FloatingActionButton>
      </div>
      </div>
    )
  }
}
function mapStateToProps(state) {
  return {
    loading: state.loading
  };
}
export default connect(mapStateToProps)(Index);

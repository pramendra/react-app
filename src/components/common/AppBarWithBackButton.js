import React, { Component } from 'react';
import { Link } from 'react-router';
import AppBar from 'material-ui/AppBar';

import IconButton from 'material-ui/IconButton';
import HardwareKeyboardArrowLeft from 'material-ui/svg-icons/hardware/keyboard-arrow-left';
import ActionOpenInBrowser from 'material-ui/svg-icons/action/open-in-browser';
import ActionSearch from 'material-ui/svg-icons/action/search';
import {grey900} from 'material-ui/styles/colors';

class AppBarWithBackButton extends Component {

  static propTypes = {
    children: React.PropTypes.any
  };

  childContextTypes = {
    muiTheme: React.PropTypes.object.isRequired,
  }

  constructor(props){
    super(props);
    this.state = {
      open: false
    };
  }

  getStyles(){
    const styles = {
      appBar: {
        display: '-webkit-flex',
        boxShadow: 'none'
      },
      title: {
        color: grey900,
        textAlign: 'center'
      }
    };
    return styles;
  }

  static contextTypes = {
    router: React.PropTypes.object.isRequired
  }

  render(){
    const styles = this.getStyles();
    return (
      <div>
        <AppBar
          title={<span style={styles.title}>{this.props.name}</span>}
          onLeftIconButtonTouchTap={() => {this.context.router.push('/')}}
          iconElementLeft={<IconButton><HardwareKeyboardArrowLeft /></IconButton>}
          iconElementRight={
            <div>
              <IconButton>
                <ActionSearch />
              </IconButton>
              <IconButton>
                <ActionOpenInBrowser />
              </IconButton>
            </div>
          }
          style={styles.appBar}/>
      </div>
    );
  }
}

AppBarWithBackButton.childContextTypes = {
  muiTheme: React.PropTypes.object.isRequired,
};

export default AppBarWithBackButton;

import React, { Component } from 'react';
import {GridList, GridTile} from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import {Link} from 'react-router';

import ActionFavoriteBorder from 'material-ui/svg-icons/action/favorite-border';

import stylesApp from './../../containers/App.css';

import {
  grey300,
  grey400,
  fullWhite
} from 'material-ui/styles/colors';

const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    border: '4px solid',
    borderColor: grey300,
    padding: '4px 0'
  },
  gridList: {
    width: '100%',
    height: '100%',
    overflowY: 'auto',
    backgroundColor: grey300
  },
  gridTile: {
    backgroundColor: fullWhite
  },
  infoWrapper:{
    padding: '0 8px',
  },
  name: {
  },
  price: {
    fontSize: 18,
    fontWeight: 'bold',
    height: 21,
  }
};
const formatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
  minimumFractionDigits: 0,
});

class ProductList extends Component {

  render (){
    return(
      <div style={styles.root}>
        <GridList
          cols={2}
          cellHeight={260}
          padding={8}
          style={styles.gridList} >
          {this.props.list.map((listitem) => {
            return (
              <GridTile
                style={styles.gridTile}
                key={listitem.id}
                containerElement={
                  <Link to = {`/detail/${listitem.id}/`}/>
                }
              >
                <img src={listitem.image} height = {210}/>
                <div style={styles.infoWrapper}>

                  <div style={styles.name}>{listitem.name}</div>

                  <div className={stylesApp.Grid + ' ' + stylesApp.GridCenter}>
                    <div style={styles.price} className={stylesApp.GridCell75}>
                      US{formatter.format(listitem.price)}
                    </div>

                    <div className={stylesApp.GridCell25}>
                      <div className={stylesApp.Grid + ' ' + stylesApp.GridCenter}>
                        <div className={stylesApp.GridCell}><ActionFavoriteBorder /></div>
                        <div className={stylesApp.GridCell}>{listitem.like_count}</div>
                      </div>
                    </div>

                  </div>
                </div>
              </GridTile>
            )
          })}
        </GridList>
      </div>
    )
  }
};

export default ProductList;

import React, { Component } from 'react';
import { Link } from 'react-router';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import SocialNotificationsNone from 'material-ui/svg-icons/social/notifications-none';
import ActionDone from 'material-ui/svg-icons/action/done';
import ActionSearch from 'material-ui/svg-icons/action/search';
import {grey300, red700} from 'material-ui/styles/colors';

class AppBarWithDrawer extends Component {

  static propTypes = {
    children: React.PropTypes.any
  };

  childContextTypes = {
    muiTheme: React.PropTypes.object.isRequired,
  }

  constructor(props){
    super(props);
    this.state = {
      open: false
    };
  }

  getStyles(){
    const styles = {
      appBar: {
        display: '-webkit-flex',
        boxShadow: 'none',
        borderBottom: '1px solid',
        borderColor: grey300
      },
      title: {
        color: red700
      }
    };
    return styles;
  }

  leftButtonTouched = () => this.setState({open: !this.state.open});

  rightButtonTouched = () => {
    window.location  = '/'
  }

  menuItemTouched = (e) => {
    this.setState({open: !this.state.open});
    switch(e.target.innerHTML){
      case 'Top':
        window.location = '/';
        break;
    }
  }

  render(){
    const styles = this.getStyles();
    return (
      <div>
        <AppBar
          title={<span style={styles.title}>Apparel</span>}
          onLeftIconButtonTouchTap={this.leftButtonTouched}
          onRightIconButtonTouchTap={this.rightButtonTouched}
          iconElementRight={
            <div>
              <IconButton>
                <ActionSearch />
              </IconButton>
              <IconButton>
                <SocialNotificationsNone />
              </IconButton>
              <IconButton>
                <ActionDone />
              </IconButton>
            </div>
          }
          style={styles.appBar}/>

      <Drawer
          open={this.state.open}
          docked={false}
          width={200}
          onRequestChange={open => this.setState({open})}>
          <MenuItem primaryText="Menu" onTouchTap={this.menuItemTouched}/>
            <MenuItem onTouchTap={this.handleClose}>About Company</MenuItem>
            <MenuItem onTouchTap={this.handleClose}>Contact</MenuItem>
        </Drawer>
      </div>
    );
  }
}

AppBarWithDrawer.childContextTypes = {
  muiTheme: React.PropTypes.object.isRequired,
};

export default AppBarWithDrawer;

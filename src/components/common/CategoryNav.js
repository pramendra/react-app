import React from 'react';
import {Tabs, Tab} from 'material-ui/Tabs';
// From https://github.com/oliviertassinari/react-swipeable-views
import SwipeableViews from 'react-swipeable-views';
import ProductList from './ProductList';

const styles = {
  headline: {
    fontSize: 24,
    paddingTop: 16,
    marginBottom: 12,
    fontWeight: 400,
  },
  slide: {
    padding: 0,
  },
  tab: {
    textTransform: 'none',
    fontWeight: 'normal'
  }
};

export default class CategoryNav extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      slideIndex: 2,
    };
  }

  handleChange = (index) => {
    this.setState({
      slideIndex: index,
    });
  };
  filterList(list, category_id){
    return list.filter(listitem => listitem.category_id === category_id);
  }
  render() {
    return (
      <div>
        <Tabs
          onChange={this.handleChange}
          value={this.state.slideIndex}
        >
        {this.props.categories.map(function(category) {
            return (
              <Tab key={"tab_"+category.id} label={category.name} value={parseInt(category.id) -1} style={styles.tab} />
            );
        }, this)}
        </Tabs>
        <SwipeableViews
          index={this.state.slideIndex}
          onChangeIndex={this.handleChange}
        >
        {this.props.categories.map(function(category) {
            return (
              <div key={"pl_"+category.id}>
                <ProductList category={category} list={category.name === 'All' ? this.props.list: this.filterList(this.props.list, parseInt(category.id))} />
              </div>
            );
        }, this)}
        </SwipeableViews>
      </div>
    );
  }
}

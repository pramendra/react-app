import React from 'react';
import CircularProgress from 'material-ui/CircularProgress';

class CircularProgressBars extends React.Component {
  render() {
    return (
      <div>
        <CircularProgress size={60} thickness={7} />
      </div>
    );
  }
}

export default CircularProgressBars;

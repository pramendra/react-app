import request from 'superagent'

const getApiGenerator = next => (route, name) => request
	.get(route)
	.end((err, res) => {
		if (err) {
			return next({
				type: `${name}_ERROR`,
				err
			})
		}

		const data = JSON.parse(res.text)
		next({
			type: `${name}_RECEIVED`,
			data
		})
	})

const dataService = store => next => action => {
	next(action)
  const getApi = getApiGenerator(next)
	switch (action.type) {
    case 'GET_LIST_DATA':
      getApi('http://localhost:5000/items', 'GET_LIST_DATA')
    break
    case 'GET_LISTITEM_DATA':
      getApi('http://localhost:5000/items/' + action.id, 'GET_LISTITEM_DATA')
    break
    case 'GET_CATEGORY_DATA':
      getApi('http://localhost:5000/categories', 'GET_CATEGORY_DATA')
    break
	default:
		break
	}
};

export default dataService

export default function list(state = [], action) {
  switch (action.type) {
	case 'GET_LIST_DATA_RECEIVED':
		return action.data.data
	default:
		return state
	}
}

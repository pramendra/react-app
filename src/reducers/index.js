import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import list from './../reducers/list';
import listitem from './../reducers/listitem';
import category from './../reducers/category';
import loading from './../reducers/loading';

module.exports = combineReducers({
  routing: routerReducer,
  list: list,
  listitem: listitem,
  categories: category,
  loading: loading
});

export default function loading(state = false, action) {
	switch (action.type) {
	case 'GET_LIST_DATA':
		return true
	case 'GET_LIST_DATA_RECEIVED':
		return false
	case 'GET_LIST_DATA_ERROR':
		return false
	default:
		return state
	}
}

export default function category(state = [], action) {
  switch (action.type) {
	case 'GET_CATEGORY_DATA_RECEIVED':
		return action.data.data
	default:
		return state
	}
}

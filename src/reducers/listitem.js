export default function list(state = {}, action) {
  switch (action.type) {
	case 'GET_LISTITEM_DATA_RECEIVED':
		return action.data
	default:
		return state
	}
}

# EC Mobile App

A react-redux-es6 project on Docker

## Dependancy

* api server

## Requirements

* npm [How to install npm](http://blog.npmjs.org/post/85484771375/how-to-install-npm)
* Docker [How to install Docker on MAC](https://docs.docker.com/engine/installation/mac/) [optional]

## Setup

### Run API Server

```
$ git clone git@github.com:pramendra/sinatra-api-server.git
$ cd sinatra-api-server.git
$ bundle install
$ bundle exec foreman start
```

#### _change log in Api server_

```
* please clone this version as apis were not working due to cross origin issue
* updated localhost to 0.0.0.0 to support docker
```

### Run App

```
$ git clone git@github.com:pramendra/react-app.git
$ cd react-app
$ npm install
$ npm run dev
browse app http://localhost:3000/ [please browser with mobile agent or device]
```



## Demo

```
please download and view 
https://github.com/pramendra/react-app/blob/master/video-demo.mov
```

